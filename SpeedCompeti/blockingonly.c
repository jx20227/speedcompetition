 #include<stdio.h>
#include<stdlib.h>
#include<omp.h>
#include<math.h>
#include<time.h>

#define CL(i,j) (i)*NMAX+(j)

void c_zero(int NMAX, double *c) {
  for (int i=0; i<NMAX; i++)  {
    for (int j=0; j<NMAX; j++)   {
      c[CL(i,j)]=0;
    }
  }
}

void c_check(int NMAX, double *c, double *c0) {
  for (int i=0; i<NMAX; i++)  {
    for (int j=0; j<NMAX; j++)   {
      if (fabs(c0[CL(i,j)]-c[CL(i,j)])>1E-5) {
        printf("error exit");
        exit(1);
      }
    }
  }
}

int main()
{
  int NMAX,i,ii,j,jj,k,kk,bi,bk,bj,x;
  double time1,time2;
  
  //enter NMAX
  NMAX = 1000;
  /*
  printf("Please enter the matrix size\n");
  if (scanf("%d",&NMAX)<1) {
      fprintf(stderr, "error\n");
      exit(1);
  }
  printf("Matrix size=%d\n",NMAX);
  */

  /* malloc1D[double,NMAX*NMAX] */
  double *a;
  if(( a=(double *)malloc(sizeof(double)*NMAX*NMAX))==NULL)    {
      printf("Out of memory, exit."); exit(1);
    }
  double *b;
  if(( b=(double *)malloc(sizeof(double)*NMAX*NMAX))==NULL)    {
      printf("Out of memory, exit."); exit(1);
    }
  double *c;
  if(( c=(double *)malloc(sizeof(double)*NMAX*NMAX))==NULL)    {
      printf("Out of memory, exit."); exit(1);
    }
  double *c0;
  if(( c0=(double *)malloc(sizeof(double)*NMAX*NMAX))==NULL)    {
      printf("Out of memory, exit."); exit(1);
    }

  for (i=0; i<NMAX; i++)   {
    for (j=0; j<NMAX; j++)   {
      a[CL(i,j)]=i+1;
      b[CL(i,j)]=j+1;
      c[CL(i,j)]=0.0;
      c0[CL(i,j)]=0.0;
    }
  }
  for (k=0; k<NMAX; k++)  {
    for (i=0; i<NMAX; i++)  {
      for (j=0; j<NMAX; j++)   {
        c[CL(i,j)]+=a[CL(i,k)]*b[CL(k,j)];
      }
    }
  }
  for (i=0; i<NMAX; i++)  {
    for (j=0; j<NMAX; j++)   {
      c0[CL(i,j)]=c[CL(i,j)];
    }
  }

  for(x=0;x<11;x++){
    //[ikj]loop
    bi=250,bk=100,bj=1000;
    c_zero(NMAX, c);
    time1=omp_get_wtime();
    for (i=0; i<NMAX; i+=bi)  {
        for (k=0; k<NMAX; k+=bk)  {
        for (j=0; j<NMAX; j+=bj)   {
            for(ii=i; ii<i+bi; ii++)    {
                for(kk=k; kk<k+bk; kk++)    {
                    for(jj=j; jj<j+bj; jj++)    {
                        c[CL(ii,jj)]+=a[CL(ii,kk)]*b[CL(kk,jj)];
                    }
                }
            }
        }
        }
    }
    time2=omp_get_wtime();
    c_check(NMAX, c, c0);
    //printf("[ikj] time=%ldsec.",time2-time1);
    printf("%10.3f\n",2.0*NMAX*NMAX*NMAX/(time2-time1)/1000000000.0);
  }

  free(a); /* Replace the line */
  free(b); /* Replace the line */
  free(c); /* Replace the line */
  return 0;
}
