/*
　　　　　　　　　　　　　　　　　＿..._
　　　　　　　　　　, -‐ ''´￣￣　　　￣￣｀ｰ- ､
　　　　　　　　 ／　　　　　　　　　　　　　　　　｀ヽ
　　　　　　　／　　　　　　　 ｀　　　　　´　　　　 ヽ
　　　　　　/　　　　　　　　　　　　　　　　　　　　　 .|
　　　　　 /　　　　　　　　 ●　　　__　　 ●　　　　 :ｌ
　　 , - ､{　:: 　　　　　 －　　 ､＿人＿,　　　ー　　:}
　　（　 :´ ::　　　　　　 ￣　　　　　　　　　　　￣　 人
　　 ゝ､ ／⌒ヽ　　　　　　　　　　　　　　　　　　.／　ヽ
　　　　（_　_＿,,,ゝ､＿＿＿＿　　　　　　 ____,,,∠＿＿,;）
　　　　　 ~　　　　　　　　　　 ￣'''''''￣￣
*/

#include<stdio.h>
#include<stdlib.h>
#include<omp.h>
#include<math.h>
#include"mkl.h"

#define CL(i,j) (i)*NMAX+(j)

void c_zero(int NMAX, double *c) {
  for (int i=0; i<NMAX; i++)  {
    for (int j=0; j<NMAX; j++)   {
      c[CL(i,j)]=0;
    }
  }
}

void c_check(int NMAX, double *c, double *c0) {
  for (int i=0; i<NMAX; i++)  {
    for (int j=0; j<NMAX; j++)   {
      if (fabs(c0[CL(i,j)]-c[CL(i,j)])>1E-5) {
        printf("error exit");
        exit(1);
      }
    }
  }
}

int main(){
  int NMAX,i,ii,j,jj,k,kk,bi,bk,bj,x;
  double time1,time2,alpha=1.0,beta=0.0;
  
  //enter NMAX
  NMAX = 2000;
  /*
  printf("Please enter the matrix size\n");
  if (scanf("%d",&NMAX)<1) {
      fprintf(stderr, "error\n");
      exit(1);
  }
  printf("Matrix size=%d\n",NMAX);
  */

  /* malloc1D[double,NMAX*NMAX] */
  double *a;
  if(( a=(double *)malloc(sizeof(double)*NMAX*NMAX))==NULL)    {
      printf("Out of memory, exit."); exit(1);
    }
  double *b;
  if(( b=(double *)malloc(sizeof(double)*NMAX*NMAX))==NULL)    {
      printf("Out of memory, exit."); exit(1);
    }
  double *c;
  if(( c=(double *)malloc(sizeof(double)*NMAX*NMAX))==NULL)    {
      printf("Out of memory, exit."); exit(1);
    }
  double *c0;
  if(( c0=(double *)malloc(sizeof(double)*NMAX*NMAX))==NULL)    {
      printf("Out of memory, exit."); exit(1);
    }

  for (i=0; i<NMAX; i++)   {
    for (j=0; j<NMAX; j++)   {
      a[CL(i,j)]=i+1;
      b[CL(i,j)]=j+1;
      c[CL(i,j)]=0.0;
      c0[CL(i,j)]=0.0;
    }
  }
  for (k=0; k<NMAX; k++)  {
    for (i=0; i<NMAX; i++)  {
      for (j=0; j<NMAX; j++)   {
        c[CL(i,j)]+=a[CL(i,k)]*b[CL(k,j)];
      }
    }
  }
  for (i=0; i<NMAX; i++)  {
    for (j=0; j<NMAX; j++)   {
      c0[CL(i,j)]=c[CL(i,j)];
    }
  }

  for(x=0;x<11;x++){
    c_zero(NMAX, c);
    time1=omp_get_wtime();
    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, NMAX, NMAX, NMAX, alpha, a, NMAX, b, NMAX, beta, c, NMAX);
    time2=omp_get_wtime();
    c_check(NMAX, c, c0);
    printf("%10.3f\n",2.0*NMAX*NMAX*NMAX/(time2-time1)/1000000000.0);
  }

  free(a); /* Replace the line */
  free(b); /* Replace the line */
  free(c); /* Replace the line */
  return 0;
}
