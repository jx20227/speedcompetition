[file]
    size
    compile option
    node
    average

Serial
[Serial.c](loop[ikj],un[441],blo[200,200,250])
    size=2000
    icc Serial.c -xHOST  -std=c99 -qopenmp -Bdynamic -Bstatic -falign-functions -falign-loops -fomit-frame-pointer -foptimize-sibling-calls 
    45.6353
    [1000]45.231
    [2000]44.4187


    size = 2000
    icc serial.c -xHost -O3 -std=c99 -qopenmp -ipo -no-prec-div
    44.5018

    size = 2000
    icc serial.c -xHost -O3 -std=c99 -qopenmp -ipo -no-prec-div -Bdynamic -Bstatic -falign-functions -falign-loops -fomit-frame-pointer -foptimize-sibling-calls 
    44.5492
    size = 1000
    45.1757



Parallel
[origin.c](loop[ikj],un[000],blo[0,0,0])
    size=2000
    mpiicc origin.c -xHost -O3 -ipo -no-prec-div -qopenmp -parallel -std=c99 -falign-functions -falign-loops -fomit-frame-pointer -foptimize-sibling-calls 
    thread=56
    2647.5591
    [1000]2541.0886
    [2000]2563.5274

    size=2000
    mpiicc origin.c -parallel -std=c99 -xHost -O3 -ipo -no-prec-div
    thread=56
    2691.4152(2707.509)
    size = 1000
    2532.0861

    size=2000
    mpiicc origin.c -parallel -std=c99 -xHost -O3 -ipo -no-prec-div
    thread=50
    2057.2436

Unlimited
[unlimited.c](loop[ikj],un[000],blo[0,0,0])
    size = 2000
    mpiicc origin.c -parallel -std=c99 -xHost -O3 -ipo -no-prec-div
    node = 4
    thread = 56
    2638.2612

[dgenmm.c]
    size = 2000
    icc -mkl dgemm.c -std=c99
    node =4 
    thread = 56
    2618.0751

    size = 2000
    icc -mkl dgemm.c -std=c99 -O3 -ipo -no-prec-div
    node = 4
    thread = 56
    2632.6952

    size = 2000
    icc -mkl dgemm.c -std=c99 -O3 -ipo -no-prec-div -qopenmp
    2622.2374

    size = 2000
    icc -mkl dgemm.c -std=c99 -O3 -ipo -no-prec-div -qopenmp -xHost
    2598.5912

    icc -mkl dgemm.c -std=c99 -O3 -ipo -no-prec-div -qopenmp -xHost -parallel
    size = 2000
    2663.183
    size = 1000    
    2804.7973

    node = 1
    [1000]2867.29
    [2000]2714.0803








コンパイラオプションまとめ？（使えそうなやつ)

Bdynamic	ランタイム時にライブラリーの動的リンクを有効にします。
Bstatic	ユーザーのライブラリーとの静的リンクを有効にします。
dll	プログラムがダイナミック・リンク・ライブラリー (DLL) としてリンクされるように指定します
dynamiclib	libtool コマンドを起動してダイナミック・ライブラリーを作成します。
dyncom、Qdyncom	ランタイム時に、共通ブロックの動的割り当てを有効にします。
f77rtl	FORTRAN 77 ランタイム動作の使用をコンパイラーに指示します。
falign-functions	プロシージャーを最適なバイト境界にアライメントするようにコンパイラーに指示します。
falign-loops	2 の累乗のバイト境界でループをアライメントします。
falign-stack	ルーチンへの入口で使用するスタック・アライメントをコンパイラーに指示します。
fast	プログラム全体の速度を最大限にします。
ffat-lto-objects	プロシージャー間の最適化 (IPO) のコンパイル (-c -ipo) で、中間言語とオブジェクト・コードの両方を含むリンク時の最適化 (LTO) 用の FAT オブジェクトを生成するかどうかを指定します。
fomit-frame-pointer、Oy	EBP を最適化で汎用レジスターとして使用するかどうかを指定します。
foptimize-sibling-calls	末尾再帰呼び出しを最適化するかどうかを指定します。
global-hoist	プログラム実行でメモリーロードをソースファイルで記述されている位置よりも先に移動できる特定の最適化を有効にします。
guide	自動ベクトル化、自動並列化、データ変換のアドバイスレベルを設定します。
guide-opts	特定のコードを解析して、最適化を向上させるためのアドバイスを生成するようにコンパイラーに指示します。
guide-par	自動並列化のアドバイスレベルを設定します。
ip	単一ファイルのコンパイルで追加のプロシージャー間の最適化を有効にするかどうかを指定します。
ip-no-inlining	プロシージャー間の最適化オプションによって有効にされた全体または部分的なインライン展開を無効にします。
ip-no-pinlining	プロシージャー間の最適化オプションによって有効にされた部分的なインライン展開を無効にします。
ipo、Qipo	複数ファイルにわたるプロシージャー間の最適化を有効にします。
ipo-c	複数ファイルにわたって最適化を行い、単一のオブジェクト・ファイルを生成するようにコンパイラーに指示します。
