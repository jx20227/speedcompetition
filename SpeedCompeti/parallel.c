#include<stdio.h>
#include<stdlib.h>
#include<omp.h>
#include<math.h>
#define CL(i,j) (i)*NMAX+(j)

void c_zero(int NMAX, double *c) {
  for (int i=0; i<NMAX; i++)  {
    for (int j=0; j<NMAX; j++)   {
      c[CL(i,j)]=0;
    }
  }
}

void c_check(int NMAX, double *c, double *c0) {
  for (int i=0; i<NMAX; i++)  {
    for (int j=0; j<NMAX; j++)   {
      if (fabs(c0[CL(i,j)]-c[CL(i,j)])>1E-5) {
        printf("error exit");
        exit(1);
      }
    }
  }
}


int main(int	argc,	char	**argv)
{
  int rank,numproc,i,j,k,NMAX,x;
  double time1,time2;
  
  NMAX =   1000;
  /* malloc1D[double,NMAX*NMAX] */
  double *a;
  if(( a=(double *)malloc(sizeof(double)*NMAX*NMAX))==NULL)    {
      printf("Out of memory, exit."); exit(1);
    }
  double *b;
  if(( b=(double *)malloc(sizeof(double)*NMAX*NMAX))==NULL)    {
      printf("Out of memory, exit."); exit(1);
    }
  double *c;
  if(( c=(double *)malloc(sizeof(double)*NMAX*NMAX))==NULL)    {
      printf("Out of memory, exit."); exit(1);
    }
    double *cmpi;
    if((cmpi =(double *)malloc(sizeof(double)*NMAX*NMAX))==NULL)    {
        printf("Out of memory, exit."); exit(1);
        } 
    double *ampi;
    if((ampi =(double *)malloc(sizeof(double)*NMAX*NMAX))==NULL)    {
        printf("Out of memory, exit."); exit(1);
        } 
    double *bmpi;
    if((bmpi =(double *)malloc(sizeof(double)*NMAX*NMAX))==NULL)    {
        printf("Out of memory, exit."); exit(1);
        }
  double *c0;
  if(( c0=(double *)malloc(sizeof(double)*NMAX*NMAX))==NULL)    {
      printf("Out of memory, exit."); exit(1);
    }

  for (i=0; i<NMAX; i++)   {
    for (j=0; j<NMAX; j++)   {
      a[CL(i,j)]=i+1;
      b[CL(i,j)]=j+1;
      c[CL(i,j)]=0.0;
      cmpi[CL(i,j)]=0.0;
      ampi[CL(i,j)]=0.0;
      bmpi[CL(i,j)]=0.0;
      c0[CL(i,j)]=0.0;
    }
  }
  for (k=0; k<NMAX; k++)  {
    for (i=0; i<NMAX; i++)  {
      for (j=0; j<NMAX; j++)   {
        c[CL(i,j)]+=a[CL(i,k)]*b[CL(k,j)];
      }
    }
  }
  for (i=0; i<NMAX; i++)  {
    for (j=0; j<NMAX; j++)   {
      c0[CL(i,j)]=c[CL(i,j)];
    }
  }

  for(int x=0; x<11;x++){
  c_zero(NMAX,c);

    time1 = omp_get_wtime();
    //[ikj]通常
    #pragma omp parallel for private(k,i,j)
    for(i=0;i<NMAX;i++){
      for(k=0;k<NMAX;k++){
          for(j=0;j<NMAX;j++){
              c[CL(i,j)]+=a[CL(i,k)]*b[CL(k,j)];
          }
      }
    }

    time2 = omp_get_wtime();

      c_check(NMAX, c, c0);
      //printf("[ikj] time=%10.3f",time2-time1);
      printf("%10.3f\n",2.0*NMAX*NMAX*NMAX/(time2-time1)/1000000000.0);
  }
  
  free(a); /* Replace the line */
  free(b); /* Replace the line */
  free(c); /* Replace the line */
  free(c0);
  free(cmpi);

  return 0;
}

